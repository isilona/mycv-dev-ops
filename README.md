-- mycv-dev-ops-policy
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecr:CreateRepository",
                "iam:CreatePolicy",
                "iam:CreateUser"
            ],
            "Resource": "*"
        }
    ]
}

-- users / mycv-dev-ops

-- users / mycv-dev-ops / Access keys

--
- AWS_DEFAULT_REGION
- AWS_ACCOUNT_ID
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

